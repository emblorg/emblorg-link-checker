# EMBL.org link checker

A script to check for broken links on the new EMBL.org

As we migrate into embl.org, sometimes links get broken or are copied already broken.

A way to test for this is being investigated with the following project:
https://www.npmjs.com/package/linkinator

## Usage

- `npm install`
- `npm start`

## Requirements 

- Node 12+  `node -v`

## Background

https://gitlab.ebi.ac.uk/emblorg/static-html-pages/issues/26